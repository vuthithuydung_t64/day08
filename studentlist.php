<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='./studentlist.css'>
    <title>Document</title>
</head>
<body>
    <?php
        session_start();

        if (isset($_POST['search']) && ($_POST['search'])) 
        {  
            if($_SESSION != $_POST){
                $_SESSION['falcuty'] = $_POST['falcuty'];
                $_SESSION['key_search'] = $_POST['key_search'];
            }   
        }

        // if (isset($_POST['delsearch']) && ($_POST['delsearch']))
        // {
        //     // if($_SESSION != $_POST){
        //         session_unset();
        //         unset($_POST);
                
        //     // }   
        // }



    ?>
    <script>
        function DeleteSearchField() {
            document.getElementById("falcuty").value = "";
            document.getElementById("key_search").value = "";
        }

        
    </script>

    <div class='container'>
      <div class='container_margin'>
        <form method='post' action='studentlist.php'>
            <table id='table_search'>
                <tr>
                    <td>Khoa</td>
                    <td> <select class='box width_resize' id='falcuty' name='falcuty' >
                      <?php
                       $falcuty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                       foreach($falcuty as $key => $value){
                           echo " <option ";
                               echo isset($_SESSION['falcuty']) && $_SESSION['falcuty'] == $key ? "selected " : "";
                           echo"                           
                           value =".$key.">".$value."</option> ";  
                       }
                
                      ?>
                        <br/>
                </tr>

                <tr>
                    <td>Từ khóa</td>
                    <td><input id='key_search' type='text' class='box width_resize' name='key_search' 
                          value=
                            <?php
                            echo isset($_SESSION['key_search']) ? $_SESSION['key_search'] : "";
                            ?> 
                        >
                    </td>
                    

                </tr>

            </table>

            <input class='box blue_bgr border_radius' type='button' id='delsearch' name='delsearch' value='Xóa' onclick='DeleteSearchField();'>

            <input class='box blue_bgr border_radius' type='submit' id='search' name='search' value='Tìm kiếm'> 
            
            <p>Số sinh viên tìm thấy: XXX</p>

            <button class='box blue_bgr border_radius' id='add' name='turnToSignup'>Thêm</button>
            <?php
                    if (isset($_POST["turnToSignup"]))
                    { 
                        header("location: signup.php");
                    }

            ?>

            <br/> 
            <table>
                <thead>
                    <td class='col1'>No</td>
                    <td class='col2'>Tên sinh viên</td>
                    <td class='col3'>Khoa</td>
                    <td class='col4'>Action</td>

                </thead>

                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td class='col4'>
                            <button class='edit_del'>Xóa</button>
                            <button class='edit_del'>Sửa</button>

                        </td>

                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td class='col4'>
                            <button class='edit_del'>Xóa</button>
                            <button class='edit_del'>Sửa</button>

                        </td>

                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học vật liệu</td>
                        <td class='col4'>
                            <button class='edit_del'>Xóa</button>
                            <button class='edit_del'>Sửa</button>

                        </td>

                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học máy tính</td>
                        <td class='col4'>
                            <button class='edit_del'>Xóa</button>
                            <button class='edit_del'>Sửa</button>

                        </td>

                    </tr>
                </tbody>

            /<table>


        </form>
      </div>
    </div>
</body>
</html>